//
//  AudioConfiguration.swift
//  InsuranceApp
//
//  Created by Ritesh Verma on 20/06/21.
//

import Foundation
import AVFoundation
import AVKit

// Audio recording
class AudioConfiguration: NSObject {
    
    // audio recording
    var audioRecorder: AVAudioRecorder!
    var audioPlayer : AVAudioPlayer!
    var isAudioRecordingGranted: Bool!
    var isRecording = false
    var isPlaying = false
    var queNumber = 1
    var isCameraRecording = false
    func getDocumentsDirectory() -> URL
    {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

    func getFileUrl() -> URL{
        let filename = "answer\(self.queNumber).m4a"
        let filePath = getDocumentsDirectory().appendingPathComponent(filename)
        return filePath
    }
    
    func getQuestionUrl()->URL{
        guard let urlPath = Bundle.main.url(forResource: "que\(self.queNumber)", withExtension: "mp3") else { return getFileUrl()
        }
        return urlPath
    }
    func setup_recorder()
    {
        if isAudioRecordingGranted
        {
            let session = AVAudioSession.sharedInstance()
            do
            {
                try? session.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.videoRecording, options: AVAudioSession.CategoryOptions.mixWithOthers)


//                try session.setCategory(AVAudioSession.Category.playAndRecord, options: [.defaultToSpeaker,.mixWithOthers])
//                try session.setMode(.videoRecording)
                try session.setActive(true)
                let settings = [
                    AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                    AVSampleRateKey: 44100,
                    AVNumberOfChannelsKey: 2,
                    AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue
                ]
                audioRecorder = try AVAudioRecorder(url: getFileUrl(), settings: settings)
                audioRecorder.prepareToRecord()
            }
            catch let error {
                print(error)
            }
        }
        else
        {
            print("error")
        }
    }
    func check_record_permission()
    {
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSession.RecordPermission.granted:
            isAudioRecordingGranted = true
            break
        case AVAudioSession.RecordPermission.denied:
            isAudioRecordingGranted = false
            break
        case AVAudioSession.RecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (allowed) in
                    if allowed {
                        self.isAudioRecordingGranted = true
                    } else {
                        self.isAudioRecordingGranted = false
                    }
            })
            break
        default:
            break
        }
    }
    
    @IBAction func start_recording(_ sender: UIButton)
    {
        if(isRecording)
        {
            finishAudioRecording(success: true)
            isRecording = false
        }
        else
        {
            setup_recorder()

            audioRecorder.record()
            isRecording = true
        }
    }

    func startRecording(){
        self.setup_recorder()
        self.audioRecorder.record()
        self.isRecording = true

    }
    
    func finishRecording(){
        self.finishAudioRecording(success: true)
        self.isRecording = false
    }
    
    func finishAudioRecording(success: Bool)
    {
        if success
        {
            if audioRecorder != nil{
                audioRecorder.stop()
            }
            audioRecorder = nil
            print("recorded successfully.")
        }
        else
        {
            print("recorded")
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool)
    {
        if !flag
        {
            finishAudioRecording(success: false)
        }
    }

    func play_recording()
    {
        if(isPlaying)
        {
            audioPlayer.stop()
            isPlaying = false
        }
        else
        {
            if FileManager.default.fileExists(atPath: getFileUrl().path)
            {
                prepare_play()
                audioPlayer.play()
                audioPlayer.delegate = self
                isPlaying = true
            }
        }
    }
    func prepare_play()
    {
        do
        {
            audioPlayer = try AVAudioPlayer(contentsOf: getFileUrl())
            audioPlayer.prepareToPlay()
        }
        catch{
            print("Error")
        }
    }
    
    
    func play_Question()
    {
        if(isPlaying)
        {
            audioPlayer.stop()
            isPlaying = false
        }
        else
        {
            prepare_play_Question()
            audioPlayer.play()
            isPlaying = true
        }
    }
    func prepare_play_Question()
    {
        do
        {
            audioPlayer = try AVAudioPlayer(contentsOf: getQuestionUrl())
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
        }
        catch{
            print("Error")
        }
    }
}

extension AudioConfiguration:AVAudioPlayerDelegate{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool){
        // Finish Playing then start recording
        if self.isCameraRecording{
            print("here we come for question number \(self.queNumber)")
            self.audioPlayer.stop()
            self.startRecording()
            DispatchQueue.main.asyncAfter(deadline: .now() + 8) {
                self.finishRecording()
            }
        }
    }
}
