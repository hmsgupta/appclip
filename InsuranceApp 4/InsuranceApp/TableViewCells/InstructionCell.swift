//
//  InstructionCell.swift
//  InsuranceApp
//
//  Created by Ritesh on 14/02/22.
//

import Foundation
import UIKit

class InstructionCellModel {
    var value = ""
    var isSelected = false
}

class InstructionCell: UITableViewCell {


    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    deinit {
        print("deinit called here")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

    func configureData(model:InstructionCellModel){
        self.titleLabel.text = model.value;
        if model.isSelected{
            self.checkImageView.image = UIImage.init(named: "check")
        }else{
            self.checkImageView.image = UIImage.init(named: "uncheck")
        }
    }
   

}

