//
//  InstructionViewController.swift
//  InsuranceApp
//
//  Created by Ritesh on 14/02/22.
//

import Foundation
import UIKit
protocol InstructionViewControllerDelegate: class  {
    func continueButtonAction()
}

class InstructionViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var startInterviewButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var cellMolel : [InstructionCellModel] = [];
    weak var delegate: InstructionViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        let model = InstructionCellModel()
        model.value = "You are in quiet location";
        cellMolel.append(model)
        
        let model1 = InstructionCellModel()
        model1.value = "Answer only in YES or NO after the beep";
        cellMolel.append(model1)
        
        let model2 = InstructionCellModel()
        model2.value = "Please use a normal speaking voice";
        cellMolel.append(model2)
        
        self.tableView?.register(UINib(nibName:String(describing: InstructionCell.self), bundle:nil), forCellReuseIdentifier: String(describing: InstructionCell.self))
        // Do any additional setup after loading the view.
//        #if APPCLIP
//        #else
//        #endif
    }
    @IBAction func startInterviewButtonAction(_ sender: Any) {
        let arr = self.cellMolel.filter({$0.isSelected==false})
        if arr.count == 0{
            self.delegate?.continueButtonAction()
        }
//         Open view view
//        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") else { return}
//        self.navigationController?.pushViewController(vc, animated: true)

    }
}

extension InstructionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellMolel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: InstructionCell.self), for: indexPath) as! InstructionCell
        cell.configureData(model: self.cellMolel[indexPath.row])
        return cell
            
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension

    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.cellMolel[indexPath.row]
        model.isSelected = !model.isSelected
        let cell = tableView.cellForRow(at: indexPath) as? InstructionCell
        cell?.configureData(model: model)
    }
    
}

