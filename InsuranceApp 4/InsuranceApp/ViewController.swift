//
//  ViewController.swift
//  InsuranceApp
//
//  Created by Ritesh Verma on 20/06/21.
//

import UIKit
import AVFoundation
import AVKit

class ViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var startInterviewButton: UIButton!
    @IBOutlet weak var playVideoButton: UIButton!
    @IBOutlet weak var playAudioButton: UIButton!
    
    var audioConfig : AudioConfiguration!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.audioConfig = AudioConfiguration()
        // Do any additional setup after loading the view.
        self.playVideoButton.setTitle("Play Video", for: .normal)
        self.playAudioButton.setTitle("Play Audio", for: .normal)
        
        #if APPCLIP
        self.titleLabel.text = "This is app clip."
        self.startInterviewButton.setTitle("Start Interview in App Clip", for: .normal)
        #else
        self.titleLabel.text = "This is complete app."
        self.startInterviewButton.setTitle("Start Interview in App", for: .normal)
        #endif
    }
    @IBAction func startInterviewButtonAction(_ sender: Any) {
        // Open view view
        let vc = CameraViewController(nibName: "CameraViewController", bundle: nil)
        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func playVideoButtonAction(_ sender: Any) {
        self.getVideofromDDandPlay()
    }
    
    @IBAction func playAudioButtonAction(_ sender: Any) {
        if self.audioConfig.isPlaying{
            self.playAudioButton.setTitle("Play Audio", for: .normal)
            self.audioConfig.play_recording()
        }else{
            self.playAudioButton.setTitle("Stop Audio", for: .normal)
            self.audioConfig.play_recording()
        }
    }
    
    func getVideofromDDandPlay(){
        let cameraConfig = CameraConfiguration()
        let newPath = cameraConfig.getFileUrl()
        let player = AVPlayer(url: newPath)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.present(playerController, animated: true) {
            player.play()
        }
    }
    
}

