//
//  DemoVideoViewController.swift
//  InsuranceApp
//
//  Created by Testing on 14/02/22.
//

import UIKit
import AVKit
import FittedSheets

class DemoVideoViewController: UIViewController {

    @IBOutlet weak var videoView1: UIView!
    @IBOutlet weak var playPauseBtn: UIButton!
    var sheet : SheetViewController?
    var avPlayer: AVPlayer?

    var isPlaying: Bool {
        return avPlayer?.rate != 0 && avPlayer?.error == nil
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        Utils.delay(0.1) {
            self.addVideoLayer()
        }
    }
    

    func addVideoLayer(){
        if let configmodel = DynamicLinkHelper.sharedInstance.configData, let questionItem = configmodel.assetsArr.first, let url = URL(string: questionItem.url){
            avPlayer = AVPlayer(url: url)
            let playerLayer = AVPlayerLayer(player: avPlayer)
        
            playerLayer.frame = videoView1.bounds
            videoView1.layer.addSublayer(playerLayer)
            avPlayer?.play()
            
            #if DEBUG
            avPlayer?.isMuted = true
            #endif
        }
    }
    

    @IBAction func startButtonAction(_ sender: Any) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "InstructionViewController") as? InstructionViewController else { return  };
        controller.delegate  = self

        sheet = SheetViewController(controller: controller, sizes: [.fixed(600), .fullScreen])
        sheet?.topCornersRadius = 20
        sheet?.dismissable = false
        sheet?.didDismiss = { [weak self] _ in
            // This is called after the sheet is dismissed
            guard let vc = self?.storyboard?.instantiateViewController(withIdentifier: "ViewController") else { return}
            self?.navigationController?.pushViewController(vc, animated: true)
        }

        self.present(sheet!, animated: true, completion: nil)

        
    }
    
    @IBAction func playPauseAction(_ sender: Any) {
        updateStatus()
        updateUI()
    }
    func updateUI() {
            if isPlaying {
                playPauseBtn.setImage(nil, for: .normal)
            } else {
                playPauseBtn.setImage(UIImage(named: "play-button"), for: .normal)
            }
        }
    private func updateStatus() {
        if isPlaying {
            avPlayer?.pause()
        } else {
            avPlayer?.play()
        }
    }
}
extension DemoVideoViewController:InstructionViewControllerDelegate{
    func continueButtonAction() {
        if let sheetVC = sheet{
            sheetVC.dismiss(animated: true)
        }

    }
    
    
}
