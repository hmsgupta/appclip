//
//  ApiEndPoints.swift
//  InsuranceApp
//
//  Created by Himanshu Gupta on 12/02/22.
//

import Foundation
import Alamofire

let baseUrl = "https://app-dev.clearspeed.ai"
let cs_api_key = "7c4c0e79-741f-4301-8dab-32a3c0ae653c"
let appVersion = "1.0"

var defaultHeaders = ["Accept-Encoding": "gzip",
                    "AppVersion": appVersion,
                    "Accept":"application/json",
                    "Content-Type" : "application/json",
                      "CSAPI": cs_api_key]


struct configEndPoints{
    let path = "\(baseUrl)/csmobile/v1/projects/config"
    let methodType: HTTPMethod = .get
    
    func getQueriesUrl(params:[String:String])->URL{
        var queryItems = [URLQueryItem]()
        for item in params{
            let queryItem = URLQueryItem(name: item.key, value: item.value)
            queryItems.append(queryItem)
        }
        var urlComps = URLComponents(string: path)!
        urlComps.queryItems = queryItems
        return urlComps.url!
    }
    func getHeaders()->[String:String]{
        return defaultHeaders
    }
}

