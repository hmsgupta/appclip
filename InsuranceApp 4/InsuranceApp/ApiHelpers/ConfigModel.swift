//
//  ConfigModel.swift
//  InsuranceApp
//
//  Created by Testing on 17/02/22.
//

import Foundation
class Item: NSObject{
    var seq_no : Int = 0
    var file_name : String = ""
    var url : String = ""
    init(_ seqNo:Int,_ fileName:String,_ url:String){
        super.init()
        self.seq_no = seqNo
        self.file_name = fileName
        self.url = url
    }
}
class ConfigModel : NSObject{

    var questionsArr = [Item]()
    var assetsArr = [Item]()
    
    init(fromDictionary dictionary: [String:Any]){
        if let qArr = dictionary["questions"] as? [[String: Any]]{
            for i in 0..<qArr.count{
                let item = qArr[i]
                if let seqNo = item["seq_no"] as? Int, let fileName = item["file_name"] as? String,let url = item["url"] as? String{
                    let itemObj = Item(seqNo , fileName, url)
                    self.questionsArr.append(itemObj)
                }
            }
        }
        if let aArr = dictionary["assets"] as? [[String: Any]]{
            for i in 0..<aArr.count{
                let item = aArr[i]
                if let seqNo = item["seq_no"] as? Int, let fileName = item["file_name"] as? String,let url = item["url"] as? String{
                    let itemObj = Item(seqNo , fileName, url)
                    self.assetsArr.append(itemObj)
                }
            }
        }
    }

}


class ApiErrorModel : NSObject{

    var status : String = ""
    var statusCode : Int = 0
    var message : String = ""
    
    init(fromDictionary dictionary: [String:Any]){
        if let status = dictionary["status"] as? String{
            self.status = status
        }
        if let statusCode = dictionary["statusCode"] as? Int{
            self.statusCode = statusCode
        }
        if let statusCode = dictionary["statusCode"] as? String{
            self.statusCode = Int(statusCode) ?? 0
        }
        if let message = dictionary["message"] as? String{
            self.message = message
        }
        
    }

}
