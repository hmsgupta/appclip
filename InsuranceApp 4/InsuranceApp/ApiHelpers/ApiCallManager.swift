//
//  ApiCallManager.swift
//  InsuranceApp
//
//  Created by Himanshu Gupta on 12/02/22.
//

import Foundation
import Alamofire

class ApiCallManager: NSObject{
    
    static let sharedInstance = ApiCallManager()
    
    func fetchInitialConfigs(queries: [String:String]){

        LoaderHelper.sharedInstance.showLoader()
        let endPoint = configEndPoints()
        Alamofire.request(endPoint.getQueriesUrl(params: queries), method: .get, encoding: JSONEncoding.default, headers:endPoint.getHeaders())
                    .responseJSON { response in
            debugPrint(response)
            LoaderHelper.sharedInstance.hideLoader()
                        if let statusCode = response.response?.statusCode, statusCode == 200{
                            if let data = response.result.value{
                                if let dataExist = data as? [String: Any]{
                                    print("api response: \(dataExist)")
                                    DynamicLinkHelper.sharedInstance.configData = ConfigModel(fromDictionary: dataExist)
                                }
                            }else{
                                BannerHelper.showBannerMessage("Some error occured while fetching information", isSuccess: .error)
                            }
                        }else{
                            if let data = response.result.value as? [String:Any], let msg = data["status"] as? String{
                                BannerHelper.showBannerMessage(msg, isSuccess: .error)}
                            else{
                                BannerHelper.showBannerMessage("Some error occured while fetching information", isSuccess: .error)
                            }
                        }
            
        }
    }
}
