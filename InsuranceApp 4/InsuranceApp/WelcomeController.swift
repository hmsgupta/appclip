//
//  WelcomeController.swift
//  InsuranceApp
//
//  Created by Ritesh on 12/02/22.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
import Alamofire
import FittedSheets

class WelcomeController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var startInterviewButton: UIButton!
    var sheet : SheetViewController?


    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        #if APPCLIP
//        #else
//        #endif
    }
    @IBAction func startInterviewButtonAction(_ sender: Any) {
        

//        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") else { return}
//        self.navigationController?.pushViewController(vc, animated: true)
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "DemoVideoViewController") else { return}
        self.navigationController?.pushViewController(vc, animated: true)

    }
}

extension WelcomeController:InstructionViewControllerDelegate{
    func continueButtonAction() {
        if let sheetVC = sheet{
            sheetVC.dismiss(animated: true)
        }

    }
    
    
}
