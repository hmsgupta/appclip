//
//  UIColor+Extension.swift
//  InsuranceApp
//
//  Created by Testing on 17/02/22.
//

import Foundation
import UIKit
extension UIColor{
    class func colorFromRGBValues(r:CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
    }
    class func bannerTitleColor() -> UIColor {
        return UIColor (red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)
    }
}
