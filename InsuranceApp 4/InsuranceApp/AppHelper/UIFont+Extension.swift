//
//  UIFont+Extension.swift
//  InsuranceApp
//
//  Created by Testing on 17/02/22.
//

import Foundation
import UIKit

extension UIFont {

    class func RobotoRegular(_ fontSize: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Regular", size: fontSize)!
    }
    class func RobotoBold(_ fontSize: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Bold", size: fontSize)!
    }
    
}
