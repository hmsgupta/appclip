//
//  Utils.swift
//  InsuranceApp
//
//  Created by Testing on 15/02/22.
//

import Foundation

class Utils : NSObject{
    class func delay(_ intervalVaule : Double, _ closure: @escaping()->()){
        DispatchQueue.main.asyncAfter(deadline: .now() + intervalVaule) {
            closure()
        }
    }
}
