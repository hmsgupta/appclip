//
//  DynamicLinkHelper.swift
//  InsuranceApp
//
//  Created by Himanshu Gupta on 12/02/22.
//

import Foundation
import UIKit

class DynamicLinkHelper: NSObject{
    
    
    static let sharedInstance = DynamicLinkHelper()
    var configData : ConfigModel?
    
    func handleTheContext(context: UIOpenURLContext?){
        //clearspeed://www.clearspeed.com/openquestionaire?tx_id=730600797&participant_uuid=cf9de006-5dde-4cd5-9e6f-b3f8a4e585fc
        var params = [String:String]()
        var path = ""
        if let urlContext = context {
            let sendingAppID = urlContext.options.sourceApplication
            let url = urlContext.url
            print("source application = \(sendingAppID ?? "Unknown")")
            print("url = \(url)")
            print("path = \(url.path)")
            path = url.path
            if let components = URLComponents.init(url: url, resolvingAgainstBaseURL: true){
                if let queryComp = components.queryItems{
                    for item in queryComp{
                        params[item.name] = item.value
                    }
                }
            }
        }
        if(path == "/openquestionaire"){
            self.openCustomQuestionaireSession(params: params)
        }
        else{
            ///handle future dynamic links
        }
    }
    func openCustomQuestionaireSession(params:[String:String]){
        ApiCallManager.sharedInstance.fetchInitialConfigs(queries: params)
    }
}
